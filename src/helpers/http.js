import axios from 'axios';

const httpClient = axios.create({
  baseURL: __API__,
  // headers: { Authorization: `Token ${authData.access_token}` },
});

export default {
  getJobs() {
    return httpClient.get('/jobs');
  },
  detailJob(id) {
    return httpClient.get(`/job/${id}`);
  },
};
